// Karma configuration

if (!('IN_NIX_SHELL' in process.env)) {
  process.env.CHROMIUM_BIN = require('puppeteer').executablePath();
}

module.exports = config => {
  config.set({
    frameworks: [
      // Reference: https://github.com/karma-runner/karma-jasmine
      // Set framework to jasmine
      'jasmine'
    ],

    browsers: [
      // Run tests using headless Chromium 
      'ChromiumHeadless'
    ],

    files: [
      'src/scripts/tests.webpack.js'
    ],

    preprocessors: {
      // add webpack as preprocessor
      'src/scripts/tests.webpack.js': ['webpack', 'sourcemap']
    },

    webpack: require('./webpack.test'),

    webpackMiddleware: {
      // webpack-dev-middleware configuration
      // i. e.
      stats: 'errors-only'
    },

    singleRun: true
  });
};
