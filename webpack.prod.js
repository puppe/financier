const { merge } = require('webpack-merge');
const webpack = require('webpack');

const CopyPlugin = require('copy-webpack-plugin');
const OfflinePlugin = require('offline-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

const common = require('./webpack/common.js');
const commonProdDev = require('./webpack/common-prod-dev')({isProd: true});

module.exports = merge(common, commonProdDev, {
  mode: 'production',
  devtool: 'source-map',
  output: {
    filename: '[name].[hash].js',
    publicPath: '/',
    chunkFilename: '[name].[hash].js',
  },
  plugins: [

    // Copy assets from the public folder
    new CopyPlugin({
      patterns: [{
        from: 'src/public',
      }],
    }),

    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': '"production"',
      }
    }),

    new OptimizeCssAssetsPlugin({
      cssProcessorOptions: { discardComments: { removeAll: true } },
      canPrint: true
    }),

    new OfflinePlugin({
      publicPath: '/',
      caches: {
        main: [':rest:']
      },
      externals: [
        '/'
      ],
      ServiceWorker: {
        events: true,
        navigateFallbackURL: '/'
      },
      AppCache: false
    }),
  ],
});
