const path = require('path');
const { merge } = require('webpack-merge');
const common = require('./webpack/common.js');

module.exports = merge(common, {
	output: null
}, {
	mode: 'development',
	devtool: 'inline-source-map',
	entry: null,
	output: {},
	module: {
		rules: [
			{
				test: /\.css$/,
				use: 'null-loader',
			},
			{
				test: /\.scss$/,
				use: 'null-loader',
			},
		],
	},
});