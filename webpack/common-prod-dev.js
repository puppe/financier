const path = require('path');

const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = function (options) {
	const { isProd = false, isDev = false } = options;
	if (isProd === isDev) {
		throw 'isProd and isDev must be different.'
	}
	return {
		entry: {
			app: './src/scripts/app.js',
		},
		output: {
			path: path.resolve(__dirname, '..', 'dist'),
		},
		module: {
			rules: [
				{
					test: /\.css$/,
					use: [
						'style-loader',
						{ loader: 'css-loader', options: { importLoaders: 1 } },
						'postcss-loader'
					]
				},
				{
					test: /\.scss$/,
					use: [
						{
							loader: MiniCssExtractPlugin.loader,
							options: {
								// only enable hot in development
								hmr: isDev,
								// if hmr does not work, this is a forceful method.
								reloadAll: true,
							},
						}, {
							loader: 'css-loader' // translates CSS into CommonJS
						}, {
							loader: 'postcss-loader'
						}, {
							loader: 'sass-loader' // compiles Sass to CSS
						}
					],
				},
			],
		},
		plugins: [
			new HtmlWebpackPlugin({
				template: './src/public/index.html',
				inject: 'body'
			}),

			new MiniCssExtractPlugin({
				// Options similar to the same options in webpackOptions.output
				// both options are optional
				filename: isDev ? '[name].css' : '[name].[hash].css',
				chunkFilename: isDev ? '[id].css' : '[id].[hash].css',
			}),
		]
	};
};