const path = require('path');
const { merge } = require('webpack-merge');

const common = require('./webpack/common.js');
const commonProdDev = require('./webpack/common-prod-dev')({isDev: true});

module.exports = merge(common, commonProdDev, {
  mode: 'development',
  devtool: 'eval-source-map',
  output: {
    filename: '[name].bundle.js',
    publicPath: 'http://localhost:8080/',
    chunkFilename: '[name].bundle.js',
  },
  devServer: {
    contentBase: path.join(__dirname, 'src', 'public'), // boolean | string | array, static file location
    compress: true, // enable gzip compression
    historyApiFallback: true, // true for index.html upon 404, object for multiple paths
    hot: true, // hot module replacement. Depends on HotModuleReplacementPlugin
    noInfo: true, // only errors & warns on hot reload
  },
  module: {
    rules: [
    ],
  },
});
