**Please note:** This is an unmaintained fork of
<https://github.com/financier-io/financier>.

<h1 align="center">financier</h1>

<p align="center">
  <a href="https://gitlab.com/financier/financier">⚙ Gitlab</a> —
  <a href="https://app.financier.io">💰 Production app</a> —
  <a href="https://staging.financier.io">Staging app</a> —
  <a href="https://staging.financier.io/docs">📗 Staging Docs</a> —
  <a href="https://trello.com/b/bXcFuXrm">📢 Trello</a> —
  <a href="https://financier.io">🕸 Website</a> —
  <a href="https://gitlab.com/financier/financier/container_registry">🐳 Docker</a>
</p>

> A web-based, offline-first app. Built with Angular 1 and PouchDB.

[![build status](https://gitlab.com/financier/financier/badges/master/build.svg)](https://gitlab.com/financier/financier/commits/master)

### Develop

```sh
npm install
npm start # starts web server on http://localhost:8080/
```

### Test

```sh
npm test
# or continuous:
npm run test-watch
```

### Lint

```sh
npm run lint
```

### Build (for production)

```sh
npm run build
```

### Run locally

TODO: This section needs to be revised! gulp is not used in this project
anymore.

```sh
gulp build
npm run-script docs # generate jsdoc documentation
node ./api
```

### Docs

Generate with `npm run docs`.

### Developing with Nix

If you are using the [Nix package
manager](https://nixos.org/manual/nix/stable/), you may use
[nix-shell](https://nixos.org/manual/nix/stable/#sec-nix-shell) to get a shell
with all necessary dependencies that are not managed by npm itself. Once you
are in the Nix shell, you can use npm like you are used to.

See also https://codeberg.org/puppe/financier-nix.
