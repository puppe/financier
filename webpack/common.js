const path = require('path');
const pJson = require('../package.json');
const webpack = require('webpack');

module.exports = {
	context: path.resolve(__dirname, '..'),
	target: 'web',
	externals: [],
	stats: 'errors-only',
	resolve: {
		// directories where to look for modules
		modules: [
			'node_modules'
		],

		// extensions that are used
		extensions: ['.webpack.js', '.web.js', '.js', '.html'],
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /(node_modules)/,
				use: {
					loader: 'babel-loader'
				}
			},
			{
				test: /\.html$/,
				use: [
					{
						loader: 'html-loader',
						options: {
							minimize: false,
						},
					},
				],
			},
			{
				// Capture eot, ttf, woff, and woff2
				test: /\.(eot|ttf|woff|woff2|svg|otf|eot)(\?v=\d+\.\d+\.\d+)?$/,
				use: 'file-loader'
			},
		],
	},
	plugins: [
		new webpack.DefinePlugin({
			VERSION: {
				number: `"${pJson.version}"`,
				date: `"${pJson.releaseDate}"`
			}
		}),
	],
};